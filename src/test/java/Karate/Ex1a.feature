Feature: Send request for google maps with location
Scenario: Verify that map data are received when sending address to the google map service
Given url 'https://reqres.in/api/users?page=2'
When method get
Then status 200
And print 'the value of response data is:', response.data
