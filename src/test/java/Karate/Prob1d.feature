Feature: 1d - Delete Request for URL - https://reqres.in/ api/users/3
Scenario: Verify that the appropriate response is 204
Given url 'https://reqres.in/api/users/3'
When method Delete
Then status 204
And match response.token == '#string'
And print 'The Response is $$ ', response