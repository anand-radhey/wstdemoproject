Feature: Send request for google maps with location
Scenario: Verify that map data are received when sending address to the google map service
Given url 'https://reqres.in/api/login'
And request {email: 'peter@klaven', password : 'cityslicka'}
When method post
Then status 200