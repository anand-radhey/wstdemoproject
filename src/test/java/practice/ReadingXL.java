package practice;

import java.io.IOException;

public class ReadingXL {
    public static void main(String[] args) throws IOException {
        String[][] datafromxl = utils.DataReaders.getExcelDataUsingPoi("src\\test\\resources\\data\\Cricket_Score_India_Eng_SA.xlsx", "India");
        for (int i = 0; i < datafromxl.length; i++) {
            for(int j = 0 ; j< datafromxl[0].length ; j++) {
                System.out.print(datafromxl[i][j] + "\t");
            }
            System.out.println();
        }

    }
}
