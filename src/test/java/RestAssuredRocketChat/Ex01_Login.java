package RestAssuredRocketChat;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Ex01_Login {
  @Test
  public void getAuthToken() {
	  RestAssured.baseURI = "http://101.53.157.237/api/v1/login";
	  RequestSpecification httpRequest = RestAssured.given();
	  
	  httpRequest.header("Content-Type", "application/json");
	  
		JSONObject requestParams = new JSONObject();
		/*{
			"username": "{{Susername}}",
			"password": "{{Spwd}}"
		}*/
		
		requestParams.put("username", "mumbai"); // Cast
		requestParams.put("password", "test123");
	
		httpRequest.body(requestParams.toJSONString());
		Response response = httpRequest.post();
		
		
		response.prettyPrint();
		
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		System.out.println(statusCode + " :: " + response.body().asString());
	  
	  
	  
	  
  }
}
