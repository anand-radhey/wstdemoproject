package sequencing;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

public class Ex02_DP {
  @Test(dataProvider = "dp")
  public void f(String v1, String v2, String v3) {
	  
	  System.out.println(" Movie Name :    "+v1);
	  System.out.println(" Director Name :    "+v2);
	  System.out.println(" Star Name :    "+v3);
	  
	  
  }

  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
    	new Object[] { "Raazi", "Meghna Gulzar", "Vicky Kaushal" },
        new Object[] { "Uri", "Aditya Dhar", "Vicky Kaushal" },
	      new Object[] { "Andhadhun", "Sriram Dhar", "Ayushman" },
	      new Object[] { "Baazigar", "Mastan", "Shah Rukh Khan"}
    	
    	/*
      new Object[] { 1, "a" },
      new Object[] { 2, "b" },*/
    };
  }
}
