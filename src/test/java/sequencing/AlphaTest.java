package sequencing;

import org.testng.annotations.Test;

public class AlphaTest {
  @Test
  public void first() {
	  //System.out.println();
	  System.out.println("INSIDE Method No. ONE");
  }
  
  @Test
  public void second() {
	  System.out.println("INSIDE Method No. TWO");
  }
  
  @Test
  public void third() {
	  System.out.println("INSIDE Method No. THREE");
  }
  
  @Test
  public void fourth() {
	  System.out.println("INSIDE Method No. FOUR");
  }
  
  @Test
  public void fifth() {
	  System.out.println("INSIDE Method No. FIVE");
  }
  
  
  
  
}
