package RestAssuredEmployee;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Ex01_GetAllEmp {
  @Test
  public void getEmployees() {
	  //RestAssured.baseURI = "http://localhost:3000";
	  //RequestSpecification req = RestAssured.given();
	  //Response res = req.get("/employees");
	  
	  Response res = given().get("http://localhost:3000/employees");
	  
	  res.prettyPrint();
	  
	  int statusCode = res.getStatusCode();
	  
	  System.out.println("Status code is : "+ statusCode);
	  System.out.println("Status Code-->"+statusCode);
      //Assert.assertEquals(statusCode ,200);
	  
	  Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
	  
	//Status Line
      String statusLine = res.getStatusLine();
      System.out.println("Status Line-->"+ statusLine);
      Assert.assertEquals(statusLine , "HTTP/1.1 200 OK");
      
      //Content Type
      String contentType= res.contentType();
      System.out.println("Content Type-->"+ contentType);
      Assert.assertEquals(contentType, "application/json");
      
     
	  
	  
	  
  }
}
